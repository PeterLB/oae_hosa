%% Generate the principal region for the cross-bispectrum M_xxy
function slice = genslice_bix(nfft)
% ii = f_1 and jj = f_2
% Find the correct maximal positive frequency(should be different for even
% and odd numbers).
nffth = floor(nfft/2+1);
slice = zeros(nffth,nfft)
slice(1,1) = 1;

for ii = 2:nffth;
    for jj = 1:ii
        if(jj <= ii && ((jj+ii-1) <= nffth))
            slice(ii,jj) = 1;
            % The permissible negative frequencies
            for kk = (nfft+2-ii):nfft
            slice(ii,kk) = 1;
            end
        end
    end
 
end