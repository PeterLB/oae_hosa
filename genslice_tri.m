
function slice = genslice_tri(nfft)
% Function to generate the principal region of the auto-trispectrum. In general ii=f_1, jj = f_2,
% kk =f_3
nffth = floor(nfft/2+1)-1;
% Compute which frequency is the maximal frequency of the system.

% The trispectrum is computed a bit different than the cross-trispectrum,
% but they work exactly the same. Here we just compute from bin "zero" instead of
% one.
slice = zeros(nfft,nfft,nfft);

for ii = 0:nffth
    for jj = 0:ii
        for kk = 0:jj
            if (ii+jj+kk) <= nffth;
         slice(ii+1,jj+1,kk+1) = 1;   
            end
            
        end
    end
end


% Compute which is the highest possible frequency
if(mod(nfft,2) == 1)
    c = 1;
else
    c =0;
end
nffthc = nffth+ c;
%

%
for ii = 1:nffth
    for jj = 1:ii
        for kk = 0:nffth-1
            if (ii+jj-(nffth-kk)) <= nffth && (ii+jj-(nffth-kk)*2) >= 0;
         slice(ii+1,jj+1,nffthc+kk+1) = 1;   
            end   
        end
    end
end
