function slice = genslice_trix(nfft)
% Function to compute the principal region of  cross-trispectrum. In general ii=f_1, jj = f_2,
% kk =f_3

% Compute which frequency is the maximal frequency of the system.
nffth = floor(nfft/2+1);

slice = zeros(nfft,nfft,nfft);

% The positive frequencies of the trispectrum is computed
% Subtract 2 to make sure we are the right place in the FFT vector, when
% multiplying frequencies. For example assume you cube the frequency at
% frequency bin 2, you would then expect it at position 4 in the FFT
% vector, so that 2*3*3-2=4

for ii = 1:nffth
    for jj = 1:ii
        for kk = 1:jj
            if (ii+jj+kk-2) <= nffth;
         slice(ii,jj,kk) = 1;   
            end
            
        end
    end
end

% Compute the maximal negative frequency

%
if(mod(nfft,2) == 1)
    c = 1;
else
    c =0;
end
nffthc = nffth+ c;

% The region given by R_1. Since the minimum negative frequency is at the
% end of the FFT vector, one should compensate for this when calculating,
% by adding 1 to the NFFT length and then finding the distance to the
% frequency bin, e.g the minimum negative frequency will be at distance 1
% from the DC bin.

for ii = 1:nffth
    for jj = 1:ii
        for kk = nffthc:nfft
            if( (((ii+jj-1) - (nfft+1-kk)) <= nffth) && (((ii+jj-1) - (nfft+1-kk)) >= 1))
                slice(ii,jj,kk) = 1;
            end
        end
    end
end


% The region given by R_2. We test for kk <= jj to make sure -f_3 >= - f_2

for ii = 1:nffth
    for jj = nffthc:nfft
       for kk =   nffthc:nfft
           if( kk <= jj);
               if( ((ii -(nfft+1-jj)-(nfft+1-kk)) >= 1) && ((ii -(nfft+1-jj)-(nfft+1-kk)) <= nffth))
                slice(ii,jj,kk) = 1;
               end
           end
       end
    end
end 