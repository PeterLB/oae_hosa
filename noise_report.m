%% For Gaussian Noise
n=150; %Number of realizations
fs=5000; %Sampling frequency
x = 0:1/fs:1-1/fs; %Generating one second time vector.
signal2 = zeros(fs,n);
f1 = 430;
f2 = 800;
for i = 1:n %Generating the signals, for n realizations
   signal2(:,i)=(cos(f1*2*pi*x+rand()*2*pi)+cos(f2*2*pi*x+rand()*2*pi)).^2; 
end

%%
signal2g=awgn(signal2,0); %Add white Gaussian noise to the signal
%%
Bgauss=bispecd((signal2)); %Calculating the bispectrum
%%
ab1 = sum(sum(abs(Bgauss))); %Sum of magnitudes
ab2= sum(sum(abs(Bgauss).^2)); %Sum of magnitudes squared
%% Generating size
[a,b] = size(Bgauss80db);
entro1 = zeros(a,b);
entro2=zeros(a,b);
%% Calculating the entropy for each location in the bispectrum

for i = 1:a
    for j = 1:b
        p1=abs(Bgauss80db(i,j))/ab1;
        p2=abs(Bgauss80db(i,j)^2)/ab2;
        if p1==0
        entro1(i,j)=0;
        entro2(i,j)=0;
        else
             entro1(i,j)=p1*log(p1);
             entro2(i,j)=p2*log(p2);
        end
    end
end


%% Calculation the entropy
P1=-sum(sum(entro1));
P2=-sum(sum(entro2));

%% For Gaussian input noise/ Chi squared 

n=150; %Number of realizations
fs=5000; %Sampling frequency
x = 0:1/fs:1-1/fs; %Generating one second time vector.
signal2 = zeros(fs,n);
f1 = 430;
f2 = 800;

for i = 1:n %Generating the signals, for n realizations
   signal2(:,i)=(cos(f1*2*pi*x)+cos(f2*2*pi*x)+cos((f1+f2)*2*pi*x));
end

%% Add noise
signal2g=awgn(signal2,0); %Add white Gaussian noise to the signal
signal2g=signal2g.^2; %The signal with noise squared.
signal2sq=signal2.^2; %The original signal with no noise squared.
r=snr(signal2sq(:,1),signal2g(:,1)-signal2sq(:,1)); %The SNR for the signal. 
%% Bispectrum
Bgaussin=bispecd((signal2g)); %Calculating the bispectrum
%%
ab1 = sum(sum(abs(Bgaussin)));%Sum of magnitudes
ab2= sum(sum(abs(Bgaussin).^2)); %Sum of magnitudes squared
%% Generating size
[a,b] = size(Bgauss80db);
entro1 = zeros(a,b);
entro2=zeros(a,b);
%% Calculating the entropy for each location in the bispectrum

for i = 1:a
    for j = 1:b
        p1=abs(Bgauss80db(i,j))/ab1;
        p2=abs(Bgauss80db(i,j)^2)/ab2;
        if p1==0
        entro1(i,j)=0;
        entro2(i,j)=0;
        else
             entro1(i,j)=p1*log(p1);
             entro2(i,j)=p2*log(p2);
        end
    end
end


%% Calculation the entropy
P1=-sum(sum(entro1))
P2=-sum(sum(entro2))


%% For custom Noise
n=150; %Number of realizations
fs=5000; %Sampling frequency
x = 0:1/fs:1-1/fs; %Generating one second time vector.
signal2 = zeros(fs,n);
signal2n=zeros(fs,n);
f1 = 430;
f2 = 800;
c=8; %Setting the range of the custom. This will set the SNR level.
m3 = 4; % target skewness
m4 = 31 ; %target kurtosis 
for i = 1:n %Generating the signals, for n realizations
    x1= 2*c*randn(1,fs); %Generating the random vector for the noise function 
    noise = transpose(MBHTM(x1,m3,m4));  %Generating noise to fit the
 %  desired distribution
    signal2(:,i)=(cos(f1*2*pi*x+rand()*2*pi)+cos(f2*2*pi*x+rand()*2*pi)).^2; 
    signal2n(:,i)=signal2(:,i)+noise; %Adding signal and noise
end
%% Check the disired noise distribution can be made
x= 2*randn(1,100000);
m3 = 0; % target skewness. NB: m3 is equal to 0 for a gaussian variable
m4 = 20 ; % target kurtosis. NB: m4 is equal to 3 for a gaussian variable
% compute the new random variable y that is non-gaussian.
noise = MBHTM(x,m3,m4);
Nbin = 10000;
fitDistEtienne(noise,Nbin,'normal');
set(gcf,'units','normalized','outerposition', [0.1 0.1 0.8 0.8])
set(gcf,'color','w')
xlabel(' x ');
ylabel('normalized pdf');
title(['m_1 = ',num2str(mean(noise),2),...
    ', m_2 = ',num2str(var(noise),2),...
    ', m_3 = ',num2str(skewness(noise),2),...
    ', m_4 = ',num2str(kurtosis(noise),2)]);
set(findall(gcf,'-property','fontSize'),'fontsize',20)

%%
r=snr(signal2(:,1),noise); %Calculating the SNR for the custom distributed noise
%%
B=bispecd((signal2)); %Calculating the bispectrum
%%
ab1 = sum(sum(abs(B)));     %Sum of magnitudes
ab2= sum(sum(abs(B).^2));   %Sum of magnitudes squared
%% Generating size
[a,b] = size(B);
entro1 = zeros(a,b);
entro2=zeros(a,b);
%% Calculating the entropy contribution for each location in the bispectrum

for i = 1:a
    for j = 1:b
        p1=abs(B(i,j))/ab1;
        p2=abs(B(i,j)^2)/ab2;
        if p1==0
        entro1(i,j)=0;
        entro2(i,j)=0;
        else
             entro1(i,j)=p1*log(p1);
             entro2(i,j)=p2*log(p2);
        end
    end
end


%% Calculation the entropy
P1=-sum(sum(entro1));
P2=-sum(sum(entro2));
