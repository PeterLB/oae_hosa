fs = 100;
t = (0:1/fs:1-1/fs).';
n = 150;
signal = zeros(length(t),n);
signal2 = zeros(length(t),n);
f1 = 1;
f2 = 3;
f3 = 10;
f4 = 12;
%%
% The f_1 and f_3 components are multiplied by 8, to normalize the
% bispectrum to one for the coupled peak.
for ii = 1:n
    phi1 = rand()*2*pi;
    phi2 = rand()*2*pi;
    signal(:,ii) = 8*cos(2*pi*f1*t+phi1) + cos(2*pi*f2*t+phi2)+cos(2*pi*(f1+f2)*t+phi1+phi2);
    signal2(:,ii) = 8*cos(2*pi*f3*t+phi1) +cos(2*pi*f4*t+phi2)+cos(2*pi*(f3+f4)*t+rand()*2*pi);
end
%%
s3 = signal+signal2;
B1 = bispecd(s3(:,1),100);
region = genslice_bi(100);
B1 = B1(1:51,1:26) .* region;
B10 = bispecd(s3(:,1:10),100) 
B10 = B10(1:51,1:26).* region;
%
B100 = bispecd(s3(:,1:100),100) 
B100 = B100(1:51,1:26) .* region;

%%
figure(1)
mesh(abs(B1))
zlim([0 1])
set(gcf, 'PaperPosition', [0 0 10 8]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [10 8]); %Set the paper to have width 5 and height 5.
saveas(gcf, 'B1', 'pdf') %Save figure
figure(2)
mesh(abs(B10))
zlim([0 1])
set(gcf, 'PaperPosition', [0 0 10 8]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [10 8]); %Set the paper to have width 5 and height 5.
saveas(gcf, 'B10', 'pdf') %Save figure
figure(3)
mesh(abs(B100))
zlim([0 1])
set(gcf, 'PaperPosition', [0 0 10 8]); %Position plot at left hand corner with width 5 and height 5.
set(gcf, 'PaperSize', [10 8]); %Set the paper to have width 5 and height 5.
saveas(gcf, 'B100', 'pdf') %Save figure

