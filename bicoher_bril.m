% Normalization of the bispectrum using the Brillinger method
function [bic,waxis] = bicoher (y,  nfft)
    [ly,nrecs] = size(y);
    nsamp = ly;
    nfft = ly;
    Bspec = zeros(nfft,nfft);
    bic  = zeros(nfft,nfft);
    Pyy  = zeros(nfft,1);
    mask = hankel([1:nfft],[nfft,1:nfft-1] );   % the hankel mask (faster)
    Yf12 = zeros(nfft,nfft);
    for k = 1:nrecs
        ys = y(:,k);
	ys = (ys - mean(ys));
	Yf = fft(ys,nfft)  / nsamp;
        CYf     = conj(Yf);
	Pyy     = Pyy + Yf .* CYf;
        Yf12(:) = CYf(mask);
        bic = bic + (Yf * Yf.') .* Yf12;
    end

    bic     = bic / nrecs;
    Pyy     = Pyy  / nrecs;
    mask(:) = Pyy(mask);
    bic = abs(bic).^2 ./ (Pyy * Pyy.' .* mask);
   
return
