%% Generate the principal region for the cross-bispectrum M_xxy
function slice = genslice_bi(nfft)


% Find the correct maximal positive frequency(should be different for even
% and odd numbers).
nffth = floor(nfft/2+1);
slice = zeros(nffth,nfft);
% ii = f_1 and jj = f_2
for ii = 1:nffth;
    for jj = 1:ii
        if(((jj+ii-1) <= nffth))
            slice(ii,jj) = 1;
            end
        end
end
