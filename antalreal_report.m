%% BISPECTRUM, number of realizations.
n=150;                          %Upper limit of realizations
n2=1;                           %Number of different runs
f1 = 43;
f2 = 107;
fs=600;                         %Sampling frequency. Should be kept low because of memory and for speed.
x = transpose(0:1/fs:1-1/fs);   %time vector
peak = zeros(n2,n);           %Vector to keep peak values
signal1 = zeros(fs,n);
%% Going through the different runs
for q =1:5
    for k = 1:n
   signal1(:,k)=cos(f1*2*pi*x+pi/52)+cos(f2*2*pi*x+rand()*2*pi)+cos((f1+f2)*2*pi*x+rand()*2*pi); %Frequnecy coupled, but not phase coupled frequencies. 
   %signal1(:,k)=awgn(signal1(:,k),-6); %Possible to add noise. 
    end
B=0;
for i= 1:n % Going through the number of realizations for the bispectrum
    Bs=bispecd(signal1(:,(1:i))); %Bispectrum of a single realization
    B=B+Bs; %The total bispectrum saved.
    B2=B/i;
     peak(q,i)= max(abs(B2(:))); %Saving the peak value
end
end
%% Plotting
figure (2)
xline=1:1:(n);
plot(xline,peak(1,:)/peak(1,1),xline,peak(2,:)/peak(2,1),xline,peak(3,:)/Antal3(1,1),xline,peak(4,:)/peak(4,1))
xlabel('Number of realizations')
ylabel('Normalized magnitude') %In respect to first peak.

%% TRISPECTRUM, number of realizations
n=150;                          %Upper limit of realizations
n2=1;                           %Number of different runs
f1 = 43;
f2 = 107;
f3=17;
fs=600;                         %Sampling frequency. Should be kept low because of memory
x = transpose(0:1/fs:1-1/fs);   %time vector
peak = zeros(n2,n);           %Vector to keep peak values
signal1 = zeros(fs,n);
val= zeros(1,300);
%%
for q =1:n2
    for k = 1:n
   signal1(:,k)=cos(f1*2*pi*x+pi/52)+cos(f2*2*pi*x+rand()*2*pi)+cos((f3)*2*pi*x+rand()*2*pi)+cos((f1+f2+f3)*2*pi*x+rand()*2*pi); %Generating frequency coupled signal. Random phase coupling.
    end
Trip=0;
for i = 1:150
    Trip=Trip+tspecd(signal1(:,i)); %Calculating the trispectrum of a single realization, and adds it to the stack.
    Tri2=abs(Trip)/i;
    for j = 1:floor(fs/2)           %Going through positive layers of the trispectrum
    val(1,j)=max(max(Tri2(:,:,j))); %Finding peak of layer
    end
    peak(q,i)= max(val(1,:));       %Saving the maximum value of all the layers.
end
end
%% Plotting
figure (2)
xline=1:1:(n);
plot(xline,peak(1,:)/peak(1,1),xline,peak(2,:)/peak(2,1),xline,peak(3,:)/Antal3(1,1),xline,peak(4,:)/peak(4,1))
xlabel('Number of realizations')
ylabel('Normalized magnitude') %In respect to first peak.