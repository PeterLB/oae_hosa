function [Tspec] = tspecd(y,nfft)
% Get the size of the signal matrix
[nrow, ncol] = size(y);

% The hankel mask is made to distribute the frequencies across a 2d grid,
% enabling the computation of the spectra using vector calculus.
     mask = hankel([1:nfft],[nfft,1:nfft-1] );

% 
Tspec = zeros(nfft,nfft,nfft);
    for jj = 1:ncol
        Xf     = fft(y(:,jj)-mean(y(:,jj)),nfft)/nrow;
        CXf    = conj(Xf);
        % Compute the 2 dimensional freq matrix( power and cross-power),
        % e.g X(f_1) * X(f_2) distributed in a grid.
        X2d = Xf * Xf.'; 
        for ii = 1:nfft 
            % ii is the layer, e.g f_3. so this matrix calculation gives
            % X(f_1) * X(f_2) * X(f_3) * X(-f_1-f_2-f_3)
             Tspec(:,:,ii) = Tspec(:,:,ii) + (Xf(ii) * X2d) .* reshape(CXf(mask), nfft, nfft);
             % A circular shift when we increase f_3 by one frequency bin,
             % to make sure the calc is correct.
             mask = circshift(mask,-1);
        end
    end
    
    
    Tspec =Tspec/ncol;
%%