% Normalization of the bispectrum using the Kim-Powers method
function Bspec = bicoher_kim2 (y,  nfft,max)
    [nsamp, nrecs] = size(y);
    
    Bspec  = zeros(max,max);
    PA     = zeros(max,max);
    MA     = zeros(max,max);
       
    for krec = 1:nrecs
        xseg   = y(:,krec);
        Xf     = fft(xseg-mean(xseg), nfft);
        CXf    = conj(Xf);      
        for jj = 1:max
        for kk = 1:jj
            if((jj+kk-1) <= floor(nfft/2+1))
                PA(jj,kk)=PA(jj,kk)+abs((Xf(jj) * Xf(kk)))^2;
                MA(jj,kk)=MA(jj,kk)+abs(CXf(jj+kk-1))^2;
                Bspec(jj,kk) =Bspec(jj,kk)+ Xf(jj) * Xf(kk) * CXf(jj+kk-1);
            end
        end
        end
    end
    % Normalization
    Bspec =  (abs(Bspec).^2) ./ (PA .* MA);



return