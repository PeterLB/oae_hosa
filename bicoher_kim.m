% Normalization of the bispectrum using the Kim-Powers method
function [Bspec,magvector] = bicoher_kim (y,  nfft)
    [nsamp, nrecs] = size(y);
    Bspec  = zeros(nfft,nfft);
    PA     = zeros(nfft,nfft);
    MA     = zeros(nfft,nfft);
   % The hankel mask is made to distribute the frequencies across a 2d grid,
   % enabling the computation of the spectra using vector calculus.
    mask = hankel([1:nfft],[nfft,1:nfft-1] );   
    for krec = 1:nrecs
        xseg   = y(:,krec);
        Xf     = fft(xseg-mean(xseg), nfft)/nsamp;
        CXf    = conj(Xf);      
        % Compute the 2 dimensional freq matrix( power and cross-power),
        % e.g X(f_1) * X(f_2) distributed in a grid.
        P = (Xf * Xf.');
        M = reshape(CXf(mask), nfft, nfft);
        Bspec  = Bspec + P .* M;
        % The lengths of the vectors are added together, instead of the
        % vectors themselves.
        PA = PA + abs(P).^2;
        MA = MA + abs(M).^2;    
    end
    % Normalization
    Bspec =  (abs(Bspec).^2) ./ (PA .* MA);



return