%% SELECT SIGNAL
signal=signalRecorded;

start=15000;
slut=start+44100;
signal=signal(start+1:slut,:); %Cut away start;
[siglength,n]=size(signal);

%% RMS filter
rmsval=zeros(1,n); %Vector for RMS Values 
for ii = 1:n
   rmsval(1,ii)=rms(signal(:,ii)); 
end
figure (1)
plot(rmsval) %Read off resonable limit
limit=0.008;
%% RMS filter 2
k = sum(rmsval < limit);
signalrms=zeros(siglength,k);
spot=1;
for ii = 1:250
    if rmsval(1,ii)<limit
        signalrms(:,spot)=signal(:,ii);
        spot=spot+1;
    end
end

%% SEGMENTATION #1 segments
seglength=2205; %Segment length
segnr=floor(siglength/seglength); %Number of segments
segnr=segnr-0;
segfinal=zeros(seglength,k*segnr); %for the non-averaged signal.
segsig=zeros(seglength,segnr);
shift=0; %If a shift is wanted in the segmentation
segnr=segnr-0; %If a shift is introduced, the amount of segments have to be reduced
for ii = 0:k-1 % Going through all realizations
    for jj = 0:segnr-1 %Going through all segments
    segsig(:,(jj+1))=signalrms((1+jj*seglength+ii*shift):((jj+1)*seglength+ii*shift),ii+1).* hann(seglength); %Windowing
    end
segfinal(:,1+(ii*segnr):((1+ii)*segnr)) = segsig; %Saving the segments in the input to the bispectrum or trispectrum
        
end
%% SEGMENTATION #2 Averaged segments
seglength=2205; %Segment length
segfinal=zeros(seglength,k); %For the averaged segments
segnr=floor(length(signal(:,1))/seglength); %Number of segments
segsig=zeros(seglength,segnr);
shift=0; %If a shift is wanted in the segmentation
segnr=segnr-0; %If a shift is introduced, the amount of segments have to be reduced
for ii = 0:k-1
    for jj = 0:segnr-1
        segsig(:,(jj+1))=signalrms((1+jj*seglength+ii*shift):((jj+1)*seglength+ii*shift),ii+1); %Segmentation 
 
    end
    segfinal(:,ii+1)=transpose(sum(transpose(segsig))).* hann(seglength);%Windowing and summing the segments for a single realization
end

%% BICOHERENCE
tic
Bc=bicoher_kim(segfinal,seglength);
toc
%% PRINCIPAL REGION
B_gen = genslice_bi(seglength); %Generating the principal region matrix.
B_gen=B_gen.*Bc(1:ceil(seglength/2),:); %Multiplying the bispectrum with the principal region
B_gen=B_gen(:,1:ceil(seglength/4)); %Removing not needed section
%% Plotting
x1=linspace(1,22050,ceil(seglength/2));
x2=linspace(1,11025,ceil(seglength/4));

%% Plotting Mesh
figure (2)
mesh(transpose(x2),x1,B_gen)
zlim([0 1])
xlim([1 22050])
ylim([1 11025])
xlabel 'f_1 (Hz)'
ylabel 'f_2 (Hz)'
zlabel '|b|^2'

%% Plotting contour
figure (3)
contour(transpose(x2),x1,B_gen)
zlim([0 1])
xlim([1 22050]) %3000 if a closer look at the region of interest is wanted
ylim([1 11025]) % --||--
xlabel 'f_1 (Hz)'
ylabel 'f_2 (Hz)'
zlabel '|b|^2'
%% TRISPECTUM
%Using the same segmentation
tic 
T= tcoher_kim_layer(segfinal,seglength,floor(seglength/2+1),51); %Layer is chosen as 51, f=1000Hz
toc
%% Plotting
x1=linspace(1,22050,ceil(seglength/2));
x2=linspace(1,22050,ceil(seglength/2));

%% Plotting Mesh of the layer
figure (2)
mesh(transpose(x2),x1,T)
zlim([0 1])
xlim([1 22050])
ylim([1 22050])
xlabel 'f_1 (Hz)'
ylabel 'f_2 (Hz)'
zlabel '|t|^2'

%% Plotting contour of the layer
figure (3)
contour(transpose(x2),x1,T)
zlim([0 1])
xlim([1 22050]) %3000 if a closer look at the region of interest is wanted
ylim([1 22050]) % --||--
xlabel 'f_1 (Hz)'
ylabel 'f_2 (Hz)'
zlabel '|t|^2'