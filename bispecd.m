function [Bspec,waxis] = bispecd (y,  nfft)
%Bispectrum estimation using the direct (fft-based) approach.
   [ly,nrecs] = size(y);     
   Bspec = zeros(nfft,nfft);
    mask = hankel([1:nfft],[nfft,1:nfft-1] );   % the hankel mask (faster)
    
    for krec = 1:nrecs
        xseg   = y(:,krec);
        Xf     = fft(xseg-mean(xseg), nfft)/ly;
        CXf    = conj(Xf);
        Bspec  = Bspec + (Xf * Xf.') .* ...
	         reshape(CXf(mask), nfft, nfft);
    end
    Bspec = Bspec/nrecs;
return
