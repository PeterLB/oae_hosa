function [Tspec] = tcoher_kim_layer(x,nfft,max,layer)
Tspec = zeros(max,max);
[nsamp,nrecs]  = size(x);
nffth = floor(nfft/2+1);
PA = zeros(max,max);
MA = zeros(max,max);
if(layer <= nffth)
    layerc = layer -1;
else
    layerc = -(nfft+1-layer);
end

for ii = 1:nrecs
    X = fft(x(:,ii)-mean(x(:,ii)),nfft)/nsamp;
    CX = conj(X);
    for jj = 1:max
        for kk = 1:jj
            if((layerc+jj+kk-1) >= 1 && (layerc+jj+kk)<nfft)
                Tspec(jj,kk) = Tspec(jj,kk)+ X(jj) * X(kk) * X(layer) * CX(layerc+jj+kk-1);
                PA(jj,kk)=PA(jj,kk)+abs((X(jj) * X(kk) * X(layer) ))^2;
                MA(jj,kk)=MA(jj,kk)+abs(CX(layerc+jj+kk-1))^2;
            end
        end
    end

end
Tspec = abs(Tspec).^2 ./  (PA .* MA);